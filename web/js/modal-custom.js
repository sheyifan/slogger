function fillModalOnClickTableRow() {
    /** @type {NodeListOf<HTMLTableRowElement>} */
    let tableRows = document.querySelectorAll("tbody tr")
    for (let tableRow of tableRows) {
        console.log(tableRow)
        tableRow.addEventListener('click', (mouseEvent) => {
            fillOnShown(tableRow)
        })
    }
}

/**
 * 2020-11-22 17:14 sheyifan Issue: fill modal on clicking table rows
 * @param {HTMLTableRowElement} row
 */
function fillOnShown(row) {
    // 2020-11-22 13:24 sheyifan Issue: fill modal on clicking table rows
    $('#rud').on('shown.bs.modal', fill())

    function fill() {
        /** @type {HTMLInputElement} */
        let dateInput = document.querySelector("#rud input[type='date']");
        /** @type {HTMLSelectElement} */
        let prioritySelector = document.querySelector("#rud select[name='priority']")
        /** @type {HTMLInputElement} */
        let todoInput = document.querySelector("#rud input[name='todo']")
        /** @type {HTMLInputElement} */
        let todoDescInput = document.querySelector("#rud input[name='description']")
        /** @type {HTMLSelectElement} */
        let labelsMultiSelector = document.querySelector("#rud select[name='labels']")
        /** @type {HTMLInputElement} */
        let riskInput = document.querySelector("#rud input[name='risk']")
        /** @type {HTMLInputElement} */
        let assertsInput = document.querySelector("#rud input[name='asserts']")
        /** @type {HTMLInputElement} */
        let stateInput = document.querySelector("#rud input[name='state']")
        /** @type {HTMLInputElement} */
        let finishDescInput = document.querySelector("#myModal input[name='finish-description']")

        let currentDate = document.querySelector("#current-date").value
        /** @type {NodeListOf<HTMLTableCellElement>} */
        let issueInfos = row.querySelectorAll("td")
        let issueItems = new IssueItems({
            priority: issueInfos.item(0).innerHTML,
            todo: issueInfos.item(1).innerHTML,
            todoDesc: issueInfos.item(2).innerHTML,
            labels: issueInfos.item(3).innerHTML,
            risk: issueInfos.item(4).innerHTML,
            asserts: issueInfos.item(5).innerHTML,
            state: Number.parseFloat(issueInfos.item(6).innerHTML),
            finishDesc: issueInfos.item(7).innerHTML
        })

        dateInput.value = currentDate
        switch (issueItems.priority) {
            case "T0":
                prioritySelector.selectedIndex = 0
                break;
            case "T1":
                prioritySelector.selectedIndex = 1
                break;
            case "T2":
                prioritySelector.selectedIndex = 2
                break;
        }
    }
}

class IssueItems {
    /**
     * 2020-11-23 sheyifan Issue: create a class for packing data of a issue
     * @param {{
     *     priority: string,
     *     todo: string,
     *     todoDesc: string,
     *     labels: string[],
     *     risk: string,
     *     asserts: string,
     *     state: number,
     *     finishDesc: string
     * }} issueItems 
     */
    constructor(issueItems) {
        this.priority = issueItems.priority
        this.todo = issueItems.todo
        this.todoDesc = issueItems.todoDesc
        this.labels = issueItems.labels
        this.risk = issueItems.risk
        this.asserts = issueItems.asserts
        this.state = issueItems.state
        this.finishDesc = issueItems.finishDesc
    }
}

/**
 * 2020-11-22 15:13 sheyifan Issue: iterate HTML selector
 * @param {HTMLSelectElement} selector 
 */
function iterateSelector(selector) {
    for (let i = 0 ; i < selector.options.length ; i++) {
        let option = selector.options[i]
        console.log(option)
    }
}