$(document).ready(function () {
	'use strict';

	Chart.defaults.global.defaultFontColor = '#75787c';
});

/**
 * 2020-10-27 10:40 sheyifan Issue: fill chart data in recent months
 * 2020-10-27 22:58 sheyifan Issue: change comment to JSDoc for type check
 * @param {{
 *     完成: number,
 *     未完成: number,
 *     labels: string[]
 * }} data
 */
function fillMonthsData(data, months) {
	// ------------------------------------------------------- //
	// Bar Chart
	// ------------------------------------------------------ //
	var BARCHARTEXMPLE    = $('#barChartExample1');
	var barChartExample = new Chart(BARCHARTEXMPLE, {
		type: 'bar',
		options: {
			scales: {
				xAxes: [{
					display: false,
					gridLines: {
						color: '#eee'
					}
				}],
				yAxes: [{
					display: false,
					gridLines: {
						color: '#eee'
					}
				}]
			},
		},
		data: {
			labels: (data.labels === undefined || data.labels == null) ? ["五月", "六月", "七月", "八月", "九月", "十月"] : data.labels,
			datasets: [
				{
					label: "完成",
					backgroundColor: [
						"rgba(134, 77, 217, 0.57)",
						"rgba(134, 77, 217, 0.57)",
						"rgba(134, 77, 217, 0.57)",
						"rgba(134, 77, 217, 0.57)",
						"rgba(134, 77, 217, 0.57)",
						"rgba(134, 77, 217, 0.57)",
						"rgba(134, 77, 217, 0.57)"
					],
					hoverBackgroundColor: [
						"rgba(134, 77, 217, 0.57)",
						"rgba(134, 77, 217, 0.57)",
						"rgba(134, 77, 217, 0.57)",
						"rgba(134, 77, 217, 0.57)",
						"rgba(134, 77, 217, 0.57)",
						"rgba(134, 77, 217, 0.57)",
						"rgba(134, 77, 217, 0.57)"
					],
					borderColor: [
						"rgba(134, 77, 217, 1)",
						"rgba(134, 77, 217, 1)",
						"rgba(134, 77, 217, 1)",
						"rgba(134, 77, 217, 1)",
						"rgba(134, 77, 217, 1)",
						"rgba(134, 77, 217, 1)",
						"rgba(134, 77, 217, 1)"
					],
					borderWidth: 1,
					data: data.完成,
				},
				{
					label: "未完成",
					backgroundColor: [
						"rgba(75, 75, 75, 0.7)",
						"rgba(75, 75, 75, 0.7)",
						"rgba(75, 75, 75, 0.7)",
						"rgba(75, 75, 75, 0.7)",
						"rgba(75, 75, 75, 0.7)",
						"rgba(75, 75, 75, 0.7)",
						"rgba(75, 75, 75, 0.7)"
					],
					hoverBackgroundColor: [
						"rgba(75, 75, 75, 0.7)",
						"rgba(75, 75, 75, 0.7)",
						"rgba(75, 75, 75, 0.7)",
						"rgba(75, 75, 75, 0.7)",
						"rgba(75, 75, 75, 0.7)",
						"rgba(75, 75, 75, 0.7)",
						"rgba(75, 75, 75, 0.7)"
					],
					borderColor: [
						"rgba(75, 75, 75, 0.7)",
						"rgba(75, 75, 75, 0.7)",
						"rgba(75, 75, 75, 0.7)",
						"rgba(75, 75, 75, 0.7)",
						"rgba(75, 75, 75, 0.7)",
						"rgba(75, 75, 75, 0.7)",
						"rgba(75, 75, 75, 0.7)"
					],
					borderWidth: 1,
					data: data.未完成,
				}
			]
		}
	});
}

/**
 * 2020-10-27 10:40 sheyifan Issue: fill chart data in recent weeks
 * 2020-10-27 22:58 sheyifan Issue: change comment to JSDoc for type check
 * @param {{
 *     本周: Array<number>,
 *     上周: Array<number>,
 *     labels: Array<string>
 * }} data
 */
function fillWeeklyData(data) {
	// ------------------------------------------------------- //
    // Line Chart show issue data in recent two weeks
    // ------------------------------------------------------ //
	var legendState = true;

    var LINECHART = $('#lineChart');
    var myLineChart = new Chart(LINECHART, {
        type: 'line',
        options: {
            scales: {
                xAxes: [{
                    display: true,
                    gridLines: {
                        display: false
                    }
                }],
                yAxes: [{
                    ticks: {
                        max: 100,
                        min: 0
                    },
                    display: true,
                    gridLines: {
                        display: false
                    }
                }]
            },
            legend: {
                display: legendState
            }
        },
        data: {
			labels: (data.labels === undefined || data.labels == null) ? ["周一", "周二", "周三", "周四", "周五", "周六", "周日"] : data.labels,
			datasets: [
				{
					label: "七日完成率",
					fill: true,
					lineTension: 0.2,
					backgroundColor: "transparent",
					borderColor: '#864DD9',
					pointBorderColor: '#864DD9',
					pointHoverBackgroundColor: '#864DD9',
					borderCapStyle: 'butt',
					borderDash: [],
					borderDashOffset: 0.0,
					borderJoinStyle: 'miter',
					borderWidth: 2,
					pointBackgroundColor: "#fff",
					pointBorderWidth: 5,
					pointHoverRadius: 5,
					pointHoverBorderColor: "#fff",
					pointHoverBorderWidth: 2,
					pointRadius: 1,
					pointHitRadius: 10,
					data: data.七日完成率,
					spanGaps: false
				},
				{
					label: "七日未完成率",
					fill: true,
					lineTension: 0.2,
					backgroundColor: "transparent",
					borderColor: "#EF8C99",
					pointBorderColor: '#EF8C99',
					pointHoverBackgroundColor: "#EF8C99",
					borderCapStyle: 'butt',
					borderDash: [],
					borderDashOffset: 0.0,
					borderJoinStyle: 'miter',
					borderWidth: 2,
					pointBackgroundColor: "#fff",
					pointBorderWidth: 5,
					pointHoverRadius: 5,
					pointHoverBorderColor: "#fff",
					pointHoverBorderWidth: 2,
					pointRadius: 1,
					pointHitRadius: 10,
					data: data.七日未完成率,
					spanGaps: false
				}
			]
		}
    });
}

/**
 * 2020-10-27 10:40 sheyifan Issue: fill chart data in recent weeks
 * 2020-10-27 22:58 sheyifan Issue: change comment to JSDoc for type check
 * @param {{
 *     完成: number,
 *     未完成: number,
 *     labels: string[]
 * }} data
 * @param {Array<string>} daily - labels of seven days
 */
function fillDailyData(data, daily) {
	// ------------------------------------------------------- //
    // Bar Chart
    // ------------------------------------------------------ //
    var BARCHARTEXMPLE    = $('#barChartExample2');
    var barChartExample = new Chart(BARCHARTEXMPLE, {
        type: 'bar',
        options: {
            scales: {
                xAxes: [{
                    display: false,
                    gridLines: {
                        color: '#eee'
                    }
                }],
                yAxes: [{
                    display: false,
                    gridLines: {
                        color: '#eee'
                    }
                }]
            },
        },
        data: {
            labels: (data.labels === undefined || data.labels == null) ? ["10月17日", "10月18日", "10月19日", "10月20日", "10月21日", "10月22日", "10月23日"] : data.labels,
            datasets: [
                {
                    label: "未完成",
                    backgroundColor: [
                        "rgba(75, 75, 75, 0.7)",
                        "rgba(75, 75, 75, 0.7)",
                        "rgba(75, 75, 75, 0.7)",
                        "rgba(75, 75, 75, 0.7)",
                        "rgba(75, 75, 75, 0.7)",
                        "rgba(75, 75, 75, 0.7)",
                        "rgba(75, 75, 75, 0.7)"
                    ],
                    hoverBackgroundColor: [
                        "rgba(75, 75, 75, 0.7)",
                        "rgba(75, 75, 75, 0.7)",
                        "rgba(75, 75, 75, 0.7)",
                        "rgba(75, 75, 75, 0.7)",
                        "rgba(75, 75, 75, 0.7)",
                        "rgba(75, 75, 75, 0.7)",
                        "rgba(75, 75, 75, 0.7)"
                    ],
                    borderColor: [
                        "rgba(75, 75, 75, 0.7)",
                        "rgba(75, 75, 75, 0.7)",
                        "rgba(75, 75, 75, 0.7)",
                        "rgba(75, 75, 75, 0.7)",
                        "rgba(75, 75, 75, 0.7)",
                        "rgba(75, 75, 75, 0.7)",
                        "rgba(75, 75, 75, 0.7)"
                    ],
                    borderWidth: 1,
                    data: data.未完成,
                },
                {
                    label: "完成",
                    backgroundColor: [
                        "rgba(238, 139, 152, 0.7)",
                        "rgba(238, 139, 152, 0.7)",
                        "rgba(238, 139, 152, 0.7)",
                        "rgba(238, 139, 152, 0.7)",
                        "rgba(238, 139, 152, 0.7)",
                        "rgba(238, 139, 152, 0.7)",
                        "rgba(238, 139, 152, 0.7)"
                    ],
                    hoverBackgroundColor: [
                        "rgba(238, 139, 152, 0.7)",
                        "rgba(238, 139, 152, 0.7)",
                        "rgba(238, 139, 152, 0.7)",
                        "rgba(238, 139, 152, 0.7)",
                        "rgba(238, 139, 152, 0.7)",
                        "rgba(238, 139, 152, 0.7)",
                        "rgba(238, 139, 152, 0.7)"
                    ],
                    borderColor: [
                        "rgba(238, 139, 152, 1)",
                        "rgba(238, 139, 152, 1)",
                        "rgba(238, 139, 152, 1)",
                        "rgba(238, 139, 152, 1)",
                        "rgba(238, 139, 152, 1)",
                        "rgba(238, 139, 152, 1)",
                        "rgba(238, 139, 152, 1)"
                    ],
                    borderWidth: 1,
                    data: data.完成,
                }
            ]
        }
    });
}
