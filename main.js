// Modules to control application life and create native browser window
// 2020-10-23 23:48 sheyifan Issue: put 'require' to top
const {app, BrowserWindow, ipcMain, Menu} = require('electron')
const path = require('path')

const sf = require('./utils/sfile')
const sd = require('./utils/sdate')

/**
 * 2020-10-29 sheyifan Issue: create window instance
 * @returns {BrowserWindow}
 */
function createWindow () {
	// Create the browser window.
	const mainWindow = new BrowserWindow({
		width: 1053,
		height: 620,
		webPreferences: {
			preload: path.join(__dirname, 'preload.js'),
			"nodeIntegration": false
		},
		"resizable": true
	})

	// 2020-10-27 23:03 Issue: hide menu bar
	Menu.setApplicationMenu(null)

	// and load the index.html of the app.
	mainWindow.loadFile('web/tables.html')
	
	return mainWindow
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
	let mainWindow = createWindow()
	let webContents = mainWindow.webContents

	// open develper tools
	// webContents.openDevTools()
	// 2020-10-24 15:39 sheyifan Issue: event 'dom-ready' will emitted each time DOM finished rendering
	webContents.on('dom-ready', (event) => {
		// 2020-10-24 16:05 sheyifan Issue: get file path of HTML in main process
		if(webContents.getURL().endsWith('index.html')) {
			
		}
	})
  
	app.on('activate', function () {
		// On macOS it's common to re-create a window in the app when the
		// dock icon is clicked and there are no other windows open.
		if (BrowserWindow.getAllWindows().length === 0) createWindow()
	})

	// 2020-10-23 23:48 sheyifan Issue: remove synchronize IPC
	ipcMain.on('need-data', (event, msg) => {
		let date = JSON.parse(msg)
		let dateObj = new Date(date.date)
		// data corresponding to date
		let dataOnDate = sd.getDataByDate(date, {'obj': dateObj})

		event.sender.send('data', dataOnDate)
	})

	ipcMain.on('get-by-date', (event, msg) => {
		let date = JSON.parse(msg)
		let dateObj = new Date(date.date)
		// data corresponding to date, no creating file or modifying data
		let dataOnDate = sd.getWithoutModify(date, {'obj': dateObj})

		event.sender.send('data', dataOnDate)
	})

	// 2020-10-27 16:55 sheyifan Issue: 
	ipcMain.on('need-overview', (event, msg) => {
		/**
		 * 2020-10-28 01:20 sheyifan Issue: apply JSDoc for type check
		 * @type {{
		 *     year: number;
		 *     month: number;
		 *     day: number;
		 *     hour: number;
		 *     minute: number;
		 *     second: number;
		 *     miliSecond: number;
		 *     date: string;
		 * }} - date is stream of Date instance (streamed by JSON.parse())
		 */
		let date = JSON.parse(msg)
		// 2020-10-28 01:30 Bug: fail to parse instance of `Date` in JSON object
		// Cause: JSON.parse() cannot apply on nested `Date` instance, which will
		// parse it into string. However, JSON.parse can apply on nested JSON.
		let dateObj = new Date(date.date)
		// data corresponding to date
		let ratioOnDate = sd.getRadioByDate(dateObj)

		event.sender.send('overview-data', ratioOnDate.today)

		require('./utils/issue').fillChart(webContents, ratioOnDate, dateObj)
	})
})

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
// 2020-11-02 sheyifan Attention: please quit application with 'cmd + q' in
// MacOS
app.on('window-all-closed', function () {
  	if (process.platform !== 'darwin') app.quit()
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
