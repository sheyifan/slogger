# 快速开始

本软件基于[Quick Start Guide](https://electronjs.org/docs/tutorial/quick-start)，是一个Electron客户端应用程序，用于日志记录、查找以及统计

**您可以参考[Electron API Demos](https://electronjs.org/#get-started)以学习Electron的开发方式**

一个最基本的Electron程序必须包含以下的文件：

- `package.json` - 指向应用程序的入口，包含依赖库的信息以及其他细节内容
- `main.js` - 启动App，并且创建MainWindow以渲染DOM，这是本应用的 **主进程**.
- `*.html` - 需要渲染的HTML。这是软件的 **渲染进程s**.

您可以在[Quick Start Guide](https://electronjs.org/docs/tutorial/quick-start)中了解到更多细节

## 使用方式

您需要在电脑上安装[Git](https://git-scm.com)和[Node.js](https://nodejs.org/en/download/) (附带[npm](http://npmjs.com))来运行本程序。安装完成之后执行以下命令：

```bash
# 克隆仓库
git clone https://gitee.com/sheyifan/slogger.git
# 进入仓库所在的目录
cd slogger
# 安装依赖
npm install
# 运行App
npm start
```

## Electron的学习资源

- [electronjs.org/docs](https://electronjs.org/docs) - Electron所有文档
- [electronjs.org/community#boilerplates](https://electronjs.org/community#boilerplates) - 社区开发的案例程序
- [electron/electron-quick-start](https://github.com/electron/electron-quick-start) - 基础的Electron应用
- [electron/simple-samples](https://github.com/electron/simple-samples) - 一些有点子的小应用
- [electron/electron-api-demos](https://github.com/electron/electron-api-demos) - 一个教会你如何使用Electron的软件应用
- [hokein/electron-sample-apps](https://github.com/hokein/electron-sample-apps) - 使用不同Electron库函数实现的软件应用

## 证书

[MIT License](LICENSE.md)
