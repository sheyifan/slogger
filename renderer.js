// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process because
// `nodeIntegration` is turned off. Use `preload.js` to
// selectively enable features needed in the rendering
// process.

/**
 * 2020-10-27 22:46 sheyifan Issue: add detail about working flow
 * @description - set BrowserWindow.webPreferences.nodeIntegration = true,
 * and link this script in HTML, then this script will run in rendering
 * process
 */
